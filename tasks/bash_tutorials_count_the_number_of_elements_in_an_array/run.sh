#!/bin/bash

declare -a a
while read line
do
    a=("${a[@]}" "$line")
done

echo ${#a[@]}
