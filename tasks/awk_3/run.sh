#!/bin/bash

awk -F' ' '{
    sm = $2 + $3 + $4;
    a=80*3;
    b=60*3;
    c=50*3;

    if (sm >= a)
        print $1 " " $2 " " $3 " " $4 " : A";
    else if (sm >= b)
        print $1 " " $2 " " $3 " " $4 " : B";
    else if (sm >= c)
        print $1 " " $2 " " $3 " " $4 " : C";
    else
        print $1 " " $2 " " $3 " " $4 " : FAIL"; }'
