#!/bin/bash

res=$( uniq -c < /dev/stdin | cut -c 7- )
sz=$(echo "$res" | wc -c)

echo "$res" | head -c $(( sz - 2 ))
