#!/bin/bash

t=""
i=0

while read line
do
    if [[ i -eq 2 ]]
    then
        printf "$t$line\n"
        t=""
        i=0
    else 
        t="$t$line\t"
        ((i++))
    fi
done

if [[ i -gt 0 ]]
then
    printf "${t::-1}\n"
fi
