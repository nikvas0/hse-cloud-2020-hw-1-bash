#!/bin/bash

declare -a a
while read line
do
    a=("${a[@]}" "$line")
done

declare -a f=( ${a[@]:3:$(expr 7 - 3 + 1)} )

echo ${f[@]}

