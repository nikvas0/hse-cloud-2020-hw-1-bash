#!/bin/bash

res=$(uniq -u < /dev/stdin)
sz=$(echo "$res" | wc -c)

echo "$res" | head -c $(( sz - 2 ))
