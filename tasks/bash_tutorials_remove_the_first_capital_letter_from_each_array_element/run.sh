#!/bin/bash

#!/bin/bash

declare -a a
while read line
do
    a=("${a[@]}" "$line")
done

declare -a f=( ${a[@]/[A-Z]/.} )

echo ${f[@]}

