#!/bin/bash

sum=0

read n

for (( i = 1; i <= n; i++ ))
do
    read number
    ((sum+=number))
done

LC_NUMERIC="C" printf "%.3f\n" $(echo "scale=5;$sum/$n" | bc)
