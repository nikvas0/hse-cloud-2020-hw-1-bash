#!/bin/bash

awk -F'\n' 'BEGIN{ORS="";}{
    if (NR % 2 == 0)
        print ";"$1"\n";
    else
        print $1;
    }'